import React, {Component} from "react";
import ReactDOM from "react-dom";

const createReactClass = require('create-react-class');

class Player extends Component {
    constructor() {
        super();
        this.state = {
            playStatus: 'play',
            currentTime: 0
        };
    }
    togglePlay() {
        let status = this.state.playStatus;
        let audio = document.getElementById('audio');
        if(status === 'play') {
            status = 'pause';
            audio.play();
            let that = this;
            setInterval(function() {
                let currentTime = audio.currentTime;
                let duration = that.props.track.duration;

                // Calculate percent of song
                let percent = (currentTime / duration) * 100 + '%';
                that.updateScrubber(percent);
                that.updateTime(currentTime);
            }, 100);
        } else {
            status = 'play';
            audio.pause();
        }
        this.setState({ playStatus: status });

    }
    render() {
        return (
            <div className="b-player">
                <div className="b-player__background"
                     style={{'backgroundImage': 'url(' + this.props.track.artwork + ')'}}></div>
                <div className="b-player__header">
                    <div className="b-player__title">Now playing</div>
                </div>
                <div className="b-player__artwork"
                     style={{'backgroundImage': 'url(' + this.props.track.artwork + ')'}}></div>
                <TrackInformation track={this.props.track} />
                <Scrubber />
                <Controls isPlaying={this.state.playStatus} onClick={this.togglePlay} />
                <Timestamps duration={this.props.track.duration} currentTime={this.state.currentTime} />
                <audio id="audio"><source src={this.props.track.source} /></audio>
            </div>
        );
    }
}

Player.defaultProps = {
    track: {
        name: "The stranger things",
        artist: "Kyle Dixon, Michael Stein",
        album: "The stranger things",
        year: 2016,
        artwork: "/img/stranger-things_1.jpg",
        duration: 192,
        source: "/upload/Kyle_Dixon_Michael_Stein_Stranger_Things.mp3"
    }
};

let TrackInformation = createReactClass({
    render: function() {
        return (
            <div className="b-player__track-information">
                <div className="b-player__name">{this.props.track.name}</div>
                <div className="b-player__artist">{this.props.track.artist}</div>
                <div className="b-player__album">{this.props.track.album}</div>
            </div>
        )
    }
});

let Scrubber = createReactClass({
    render: function() {
        return (
            <div className="b-player__scrubber">
                <div className="b-player__scrubber-progress"></div>
            </div>
        )
    }
});

let Controls = createReactClass({
    render: function() {
        return (
            <div className="b-player__controls">
                <div className="b-player__btn">
                    <img className="b-player__btn-play" src="/img/play-button.svg"/>
                    <img className="b-player__btn-pause" src="/img/pause-button.svg"/>
                </div>
            </div>
        )
    }
});

let Timestamps = createReactClass({
    render: function() {
        return (
            <div className="b-player__timestamps">
                <div className="b-player__time Time--current">0</div>
                <div className="b-player__time Time--total">192</div>
            </div>
        )
    }
});

export default Player;

const wrapper = document.getElementById("Player");
wrapper ? ReactDOM.render(<Player />, wrapper) : false;